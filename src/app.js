const express = require('express');
const cors = require('cors');
const app = express();
const port = 8080;
const logger = require('morgan');
const fs = require('fs');
const path = require('path')

const filesDir = './files';

// config
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.post('/api/files', (req, res) => {
    const regex = /^([a-zA-Z0-9._-])+(.log|.txt|.json|.yaml|.xml|.js)$/g;
    const isValidFormat = regex.test(req.body.filename);
    const filename = req.body.filename;
    const content = req.body.content;

    if (!fs.existsSync(filesDir)){
        fs.mkdirSync(filesDir);
    }

    const filepath = `${filesDir}/${filename}`;

    if (filename && content && isValidFormat) {
        fs.writeFile(filepath, `${content}`, (err) => {
            if (err) {
                res.status(500).send( {
                    message: `Server error ${err}`
                })
            }
            res.status(200).send({
                message: 'File created successfully'
            });
        });
    } else {
        let invalidColumn = filename && isValidFormat ? 'content' : 'filename';

        res.status(400).send({
            message: `Please specify '${invalidColumn}' parameter`
        });
    }
});

app.get('/api/files', (req, res) => {
    try {
        res.status(200).send({
            message: 'Success',
            files: fs.readdirSync(filesDir)
        });
    } catch (e) {
        res.status(400).send({
            message: 'Client error'
        });
    }
});

app.get('/api/files/:filename', (req, res) => {
    const filename = req.params.filename;
    const filePath = `${filesDir}/${filename}`;
    fs.readFile(filePath, 'UTF-8', (err, data) => {
        if (err) {
            res.status(400).send({
                message: `No file with '${filename}' filename found`
            });
        } else {
            // get file details
            const stat = fs.statSync(filePath);
            res.status(200).send({
                message: 'Success',
                filename: filename,
                content: data,
                extension: path.extname(filename).slice(1),
                uploadedDate: stat.mtime
            })
        }
    })
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`)
})